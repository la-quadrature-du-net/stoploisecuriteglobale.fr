#!/usr/bin/env bash

set -e

cd ./img/28-novembre/
rm thumb-*.jpg || true

for image in *.jpg; do
    echo "Converting ${image}..."
    convert -define jpeg:size=330x330 "${image}" -thumbnail 330x330^ -gravity center -extent 330x330 "thumb-${image}"
done