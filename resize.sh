#!/usr/bin/env bash

set -e

cd ./img/28-novembre/
rm thumb-*.jpg || true

find . -name '*.*' -exec sh -c 'a=$(echo "$0" | sed -r "s/([^.]*)\$/\L\1/"); [ "$a" != "$0" ] && mv "$0" "$a" ' {} \;

for image in *.jpg; do
    echo "Resizing ${image}..."
    convert -auto-orient -resize 1920x ${image} ${image}
done